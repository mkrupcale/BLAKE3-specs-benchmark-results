# BLAKE3-specs-benchmark-results

## Introduction

`BLAKE3-specs-benchmark-results` is a repository storing the benchmark results for `BLAKE3-specs`[1] benchmarks[2].

## Installation

To install, simply clone this repo:

```shell
git clone https://gitlab.com/mkrupcale/BLAKE3-specs-benchmark-results.git
cd BLAKE3-specs-benchmark-results
```

### Requirements

By itself this repository has no requirements, but running the benchmarks and analysis has the same requirements as those of `BLAKE3-specs` benchmarks.

## Results

[Criterion reports](https://mkrupcale.gitlab.io/BLAKE3-specs-benchmark-results)

## License

The contents of this repository are licensed under the MIT license, except where otherwise noted. Figures and HTML, in particular, are licensed under the Creative Commons Attribution-ShareAlike (CC BY-SA) 4.0 license.

## References

1. [`BLAKE3-specs`](https://github.com/BLAKE3-team/BLAKE3-specs)
2. [`BLAKE3-specs` benchmarks](https://github.com/BLAKE3-team/BLAKE3-specs/tree/master/benchmarks)
